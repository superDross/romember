# Emulators

## All

### RetroArch

Save: `.srm`
Save Dir: same as rom
State: `.state`
State Dir: same as rom
Platforms: everything

On linux/rpi the save files and states are stored within the same directory as the roms.

On Android they are stored in a specific directories `/storage/emulated/0/RetroArch/saves` and `/storage/emulated/0/RetroArch/States`

## PSX

### PCSXR

Save: `.mcr` e.g. `card1.mcr`
Save Dir: `~/.pcsxr/memcards/`
State: `<game-id>.<slot-number>` e.g. `MARVEL_VS_CAPCOM-SLUS01059.010`
State Dir: `~/.pcsxr/sstates/`
Platforms: linux/windows/osx
Configs: `~/.pcsxr/`

#### Save Conversion

EXTENSION RENAME WORKS

Changing an `.srm` to `card1.mcr` works however an `.srm` only contains one games save, so you end up overwriting an entire memcard worth of saves for the sake of one game.

I wonder if there is a method to combine multiple `.srm` into one `.mcr`?

#### State Conversion

Doesn't work

### ePSXe

Save: `.mcr` e.g. `epsxe000.mcr`
Save Dir: `~/.epsxe/memcards/`
State: `<game-id>.<slot-number>` e.g. `SLUS_010.59HLE.000`
State Dir: `~/.epsxe/sstates/`
Platforms: linux/windows/osx
Configs: `~/.epsxe/`

#### Save Conversion

EXTENSION RENAME WORKS

Same problem as PCSXR

#### State Conversion

Doesn't work

### FPse

Save: `.mcd` e.g. `slot.mcd`
State: `.sav`

## GBA
### mGBA

Save: `.sav`
Save Dir: same as rom
State: `<rom-name>.<state-slot>` e.g. `Pokemon - Yellow Version (UE) [C][!].ss1`
State Dir: same as rom

#### Save Conversion

EXTENSION RENAME WORKS

#### State Conversion

Doesn't work

### gpSP

Save: `.sav`
State
