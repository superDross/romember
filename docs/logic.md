# Romember
## Old Approach
1 - filter DIRECTORIES table for a given user

    - List of Dirs

2 - further filter for dirs available on users in use machine OR filter for those with the users machine id

    - List of Dirs on Local Machine

3 - iterate through each valid dir entry

4 - use the emulator field in the dir entry to query the EMULATORS table and get a valid save extension

    - Valid Save Extension

5 - use the emulator field in the dir entry to query the EMULATORS table to get a corresponding console

    - Console Name

5 - use the path field in the dir entry to filter all files with a valid save extension

    - List of Local Save Files

6 - filter SAVES table for a given user and console
    - List of Server Save Files

7 - iterate through each local save file and check if it is present in the server side files (disregard there extensions)

8 - if not present upload a new entry into the SAVE table.
    - construct an AWS path
    - use the user, filename, aws path, local date modified, console name (above)

9 - use the EMULATOR table to see if they belong to the same console, if so continue.

10 - if present, the local modification time is newer than the table entries last upload and the generated checksum is different, overwrite SAVE entry.
    - change the filename, aws path? (extension), last upload date, last upload dir, emulator and check sum
11 - if present and the server modification time is newer, use the server side extension and local extension to filter for `from` and `to` in the CONVERSION table and extract the matches method
    - Conversion Method
    - change SAVE entries last download dir and last download time
12 - apply conversion method and overwrite local file.

13 - check for any server side SAVE files that are not present locally and download them.

Limit the number of directories per user to 25

## Client Application
The below approach was designed to do as much computation on the server side as possible.

**Pros**
- This ensures that the client code is as simple as possible, which will be useful when creating client side applications in unfamiliar languages.
- The server will be more powerful than the clients computer leading to faster task completion.

**Cons**
- More requests to the server for each sync execution than I would prefer.

### First Install

- Create a service to initiate the application at start up (unknown solution)
    - _Linux_: use the XDG Autostart specification
    - _Windows_: create a shortcut in the `shell:startup` folder referring to your application
- Generate a unique machine-id with `uuid.uuid1()` and store in a local config file.
    - **not sure if this is the best approach** there may be a better way to get hardware ids. Perhaps look at how closed source apps do it for for licensing purposes - they explicitly ask you to enter the machine id.
    - might also be worth storing the hostname, os and os-version/distro for a human readable alternative (but don't be dependant upon it for DB operations)
- Asks for login/registration details and send them to the API, which after registering, will send back a token `django-rest-auth` that should work indefinitely (perhaps store token in local database).

```json
POST /api/v1/register/

{
  "first-name": "david",
  "last-name": "ross",
  "email": "david@email.com",
  "password": "somepassword",
}
```

```json
POST /api/v1/login/

{
  "email": "david@email.com",
  "password": "somepassword"
}
```

```json
RESPONSE

{
  "token": "9944b09......"
}
```

- The local config file will look like this:
  - _Linux_: `~/.config/.romember.cfg`
  - _Windows_: ?

```cfg
[ID]
email: david@email.com
token: 9944b09......
machine_id: jfjfjfj989898
```

- Ask the user to mark directories containing the save files and what emulator saves it contains
  - _restrictions_: Only one emulator can be marked per computer e.g. 2 dirs cannot be marked as retroarch on the same device, customer can have no more than 15 dirs stored in the database.
  - Can include nested dirs e.g. a dir containing dirs with system names like in RetroPie
- Post these dirs to the API.
  - perhaps a way to place the machine-id in the request header? It being present in every JSON is wasteful

```json
POST /api/v1/dirs/

# HEADER - token and machine id
'Authorization: Token 9944b09...'
'X-Device: jfjfjfj989898'

[
  {
    "owner": "davidr",
    "path": "/path/to/retroarch/save-dir/",
    "emulator": "retroarch",
  },
  {
    "owner": "davidr",
    "path": "/path/to/mgba/save-dir/",
    "emulator": "mgba",
  },
  {
    "owner": "davidr",
    "path": "/path/to/drastic/save-dir/",
    "emulator": "drastic",
  }
]
```

### Primary Operation
- Make a GET request to the server every minute or so.
- Get the device data by parsing the machine id

```json
GET /api/v1/devices/<machine_id>

{}
```

```json
RESPONSE
 
[
  {
    "id": 1,
    "owner": "http://testserver/api/v1/users/zebra/",
    "machine_id": "ec97938a-c506-11e9-8c16-6057180bef5a",
    "hostname": "pc-zebra",
    "os": "windows",
    "dist": "10",
    "directories_set": [
        {
            "device": "http://testserver/api/v1/devices/ec97938a-c506-11e9-8c16-6057180bef5a/",
            "emulator": "retroarch",
            "id": 1,
            "owner": "http://testserver/api/v1/users/zebra/",
            "path": "C:\\retropie\\roms",
            "savefiles_set": [
                  {
                      "deleted": False,
                      "directory": "http://testserver/api/v1/dirs/1/",
                      "download": "http://testserver/api/v1/savefiles/2/download/",
                      "download_time": None,
                      "file": None,
                      "id": 2,
                      "md5": "2",
                      "owner": "http://testserver/api/v1/users/zebra/",
                      "self": "http://testserver/api/v1/savefiles/2/",
                      "title": "Crash Bandicoot",
                      "upload_time": "2019-08-22T18:01:54.214000Z",
                  },
                  {
                      "deleted": False,
                      "directory": "http://testserver/api/v1/dirs/1/",
                      "download": "http://testserver/api/v1/savefiles/3/download/",
                      "download_time": None,
                      "file": None,
                      "id": 3,
                      "md5": "3",
                      "owner": "http://testserver/api/v1/users/zebra/",
                      "self": "http://testserver/api/v1/savefiles/3/",
                      "title": "Mega Man V",
                      "upload_time": "2019-08-22T18:01:54.220000Z",
                  }
            ]

        },
        {
            "device": "http://testserver/api/v1/devices/ec97938b-c506-11e9-8c16-6057180bef5a/",
            "emulator": "retroarch",
            "id": 2,
            "owner": "http://testserver/api/v1/users/zebra/",
            "path": "/home/retropie/roms/",
            "savefiles_set": [
                  {
                      "deleted": False,
                      "directory": "http://testserver/api/v1/dirs/2/",
                      "download": "http://testserver/api/v1/savefiles/4/download/",
                      "download_time": None,
                      "file": None,
                      "id": 4,
                      "md5": "4",
                      "owner": "http://testserver/api/v1/users/zebra/",
                      "self": "http://testserver/api/v1/savefiles/4/",
                      "title": "Marvel vs Capcom",
                      "upload_time": "2019-08-22T18:01:54.225000Z",
                  }
            ]
        }
    ]
  }
]
```
- Iterate through each `directory_set` and the nested `savefile_set`
- Use the `path` and the savefiles title to find the file locally
- Get the local files mtime (`path.stat().st_mtime`) and compare it to the savefiles `upload_time` 


DOWNLOAD

- If the `upload_time` is newer make a get request to the `download` link
- OR if the file is not present locally

```JSON
GET /api/v1/savefiles/3/download/

{}
```

```json
RESPONSE

{ "filename": "Mega Man V.srm", "file": <byte-string>, "md5": "hhhjjh"}
```

- Save the `file` to the path using the `path` and `filename`
- Perform checksum on downloaded file and compare to response, make sure it is the same


UPLOAD

- If the `upload_time` is older, make a put request to the save files `self`
- Make sure to create a checksum before doing so

```python

url = "http://127.0.0.1:8000/api/v1/savefiles/4/"

headers = {
    "Authorization": "Token 16b1d048865bff4dcd6def7fd74e461c1254f7e9",
}

files = {"file": open(filepath, "rb")}

data = {
    "md5": "dkdkdjf",
}
r = requests.put(url, headers=headers, data=data, files=files)
```


DELETE

- If the file is not locally present, make a delete request

```json
DELETE /api/v1/savefiles/3/
```

- If a file is marked as `deleted == True` in response then delete local file

```python
f = pathlib.Path("/path/to/mgba/save-dir/Sonic_Advance_USA.sav")
f.unlink()
```


- Iterate through the local directories using the `path` value and compare the `upload_time` with local changes

UPLOAD

- If a file is present locally but not in the cloud then make a post request; save as the above put request but using `post`



### Cherry Pick
Download specific save files of a game and make it the most recent save entry for that game in the DB.

- List all save files (just call the devices endpoint)
- Select one and it show you the last 5 versions (use the `savefiles/1/history/` endpoint


### Register Directory
Register directories in the same way you would after first installing the app.

## Web App
### Endpoints
#### register

```
/api/v1/register/
```

**`POST`** creates a user

```json
REQUEST

{
  "first-name": "david",
  "last-name": "ross",
  "email": "david@email.com",
  "password": "somepassword",
}
```

#### login

```
/api/v1/login/
```

**`POST`** creates and/or sends a token back and stores the token in the users table

```json
REQUEST

{
  "email": "david@email.com",
  "password": "somepassword"
}
```

```json
RESPONSE

{
  "token": "9944b09..."
}
```

#### dirs

```
/api/v1/dirs/
```

**`POST`** store the paths in the users directories table for the given machine id

```json
REQUEST

'Authorization: Token 9944b09...'
'X-Device: jfjfjfj989898'

[
  {
    "owner": "davidr",
    "path": "/path/to/retroarch/save-dir/",
    "emulator": "retroarch",
  },
  {
    "owner": "davidr",
    "path": "/path/to/mgba/save-dir/",
    "emulator": "mgba",
  },
  {
    "owner": "davidr",
    "path": "/path/to/drastic/save-dir/",
    "emulator": "drastic",
  }
]
```

```
RESPONSE

something to just say success like 200
```

**`GET`** return all directory paths for the given machine id

```json
REQUEST

'Authorization: Token 9944b09...'
'X-Device: jfjfjfj989898'

{}
```

```
RESPONSE

[
  {
    "owner": "davidr",
    "path": "/path/to/retroarch/save-dir/",
    "emulator": "retroarch",
  },
  {
    "owner": "davidr",
    "path": "/path/to/mgba/save-dir/",
    "emulator": "mgba",
  },
  {
    "owner": "davidr",
    "path": "/path/to/drastic/save-dir/",
    "emulator": "drastic",
  }
]
```

#### actions

```
/api/v1/actions/
```

**`GET`** returns an action describing whether to upload, download or delete the file

- Cross reference the given files with the users save files table.

- If the file is not present, or if the entry in the table is older, send back an action to upload the file.

- If the entry in the table is newer, send back an action to download the file.

- If the entry in the table is marked as deleted, send back an action to delete the file.


```
REQUEST

'Authorization: Token 9944b09...'
'X-Device: jfjfjfj989898'

[
  {
    "dir": "/path/to/retroarch/save-dir/",
    "file-path": "/path/to/retroarch/save-dir/Final_Fantasy_VI_EU.srm",
    "mtime": "1556868237.728466"
  },
  {
    "dir": "/path/to/mgba/save-dir/",
    "file-path": "/path/to/mgba/save-dir/Sonic_Advance_USA.sav",
    "mtime": "1451218983.279387"
  },
  {
    "dir": "/path/to/drastic/save-dir/",
    "file-path": "/path/to/drastic/save-dir/New_Super_Mario_Bros_JP.dsv",
    "mtime": "1451218983.279387"
  }
]
```

```
RESPONSE

[
  {
    "file-path": "/path/to/retroarch/save-dir/Final_Fantasy_VI_EU.srm",
    "action": "upload"
  },
  {
    "file-path": "/path/to/mgba/save-dir/Sonic_Advance_USA.sav",
    "action": "delete"
  },
  {
    "file-path": "/path/to/drastic/save-dir/New_Super_Mario_Bros_JP.dsv",
    "action": "download"
  }
]
```

#### sync

```
/api/v1/sync/
```

**`GET`** send the requested file 

- Use DRF FileResponse class to do this

- Cross reference the users save files table and send back the file

```json
REQUEST

{
  "file": "New_Super_Mario_Bros_JP",
}
```

```json
RESPONSE

not sure how to send a file yet but md5 will need to sent too
```

**`POST`** store the file in an S3 bucket and add its metadata to the save files table

- Make sure their are a total of 5 snapshots of the same file at any time. You may have to rename these files when storing them; change suffix to be `_BU2.`.

- Use the DRF FileUploadParser class

- Perform checksum. If it fails send back an error response.

- Cross reference the users save files table and change old file names with BU suffix.

- Store the file in S3

- Add new save file entry

```json
REQUEST

{
  "filename": "/path/to/retroarch/save-dir/Final_Fantasy_VI_EU.srm",
  "dir": "/path/to/retroarch/save-dir/",
  "md5": "24ea1163ea6c9f5dae77de8c49ee7c03"
}
```

```json
RESPONSE

success status code
```
