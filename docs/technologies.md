Phase 1
------
Django
Django Rest Framework
S3 Bucket
TKinter/PyAutoGui
Postgres
PyTest

Phase 2
-------
Docker
CI/CD 
Amazon SQS/RabbitMQ

Phase 3
------
AWS stack
Kubernetes

Phase 4
-------
CSS/HTML/JS
