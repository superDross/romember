from .base_settings import *

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "romember",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "postgres",
        "PORT": "",
    }
}

AWS_STORAGE_BUCKET_NAME = "romember-savefiles"
