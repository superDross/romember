from rest_framework import serializers

from romember.utils import get_users_model


class CustomHyperLinkedRelatedField(serializers.HyperlinkedRelatedField):
    """
    HyperLinkedRelatedField that only shows a users their own objects,
    unless they are a superuser.
    """

    def __init__(self, model, **kwargs):
        self.model = model
        super().__init__(**kwargs)

    def get_queryset(self, *args, **kwargs):
        user = self.context["request"].user
        return get_users_model(self.model, user)


class SaveFileDownloadField(serializers.FileField):
    def to_representation(self, value):
        """
        Returns a temporary download link.
        """
        if not value:
            return None
        download_link = value.storage.url(value.file.name)
        return download_link
        # f = value.file.read()
        # encoded_file = base64.b64encode(f)
        # return encoded_file
