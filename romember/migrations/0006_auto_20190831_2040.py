# Generated by Django 2.2.4 on 2019-08-31 20:40

from django.db import migrations, models
import romember.storage


class Migration(migrations.Migration):

    dependencies = [
        ('romember', '0005_auto_20190831_1944'),
    ]

    operations = [
        migrations.RenameField(
            model_name='savefiles',
            old_name='name',
            new_name='title',
        ),
        migrations.AlterField(
            model_name='savefiles',
            name='file',
            field=models.FileField(storage=romember.storage.MediaStorage(), upload_to=''),
        ),
    ]
