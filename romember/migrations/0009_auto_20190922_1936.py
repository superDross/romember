# Generated by Django 2.2.4 on 2019-09-22 19:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("romember", "0008_auto_20190915_0857")]

    operations = [
        migrations.AddField(
            model_name="savefiles",
            name="directories",
            field=models.ManyToManyField(to="romember.Directories"),
        ),
        migrations.AlterField(
            model_name="emulator",
            name="console",
            field=models.CharField(
                choices=[
                    ("2600", "2600"),
                    ("32x", "32x"),
                    ("3do", "3do"),
                    ("5200", "5200"),
                    ("7800", "7800"),
                    ("all", "all"),
                    ("amstrad cpc", "amstrad cpc"),
                    ("arcade", "arcade"),
                    ("colecovision", "colecovision"),
                    ("dreamcast", "dreamcast"),
                    ("famicom disk system", "famicom disk system"),
                    ("flacon", "flacon"),
                    ("game boy advance", "game boy advance"),
                    ("game boy color", "game boy color"),
                    ("game boy", "game boy"),
                    ("gamecube", "gamecube"),
                    ("jaguar", "jaguar"),
                    ("lynx", "lynx"),
                    ("mega drive", "mega drive"),
                    ("nec pc-98", "nec pc-98"),
                    ("neo geo", "neo geo"),
                    ("nintendo 3ds", "nintendo 3ds"),
                    ("nintendo 64", "nintendo 64"),
                    ("nintendo ds", "nintendo ds"),
                    ("nintendo entertainment system", "nintendo entertainment system"),
                    ("odyssey", "odyssey"),
                    ("pc-fx", "pc-fx"),
                    ("playstation portable", "playstation portable"),
                    ("playstation", "playstation"),
                    (
                        "super nintendo entertaiment system",
                        "super nintendo entertaiment system",
                    ),
                    ("turbograx-16", "turbograx-16"),
                    ("turbograx-cd", "turbograx-cd"),
                    ("vectrex", "vectrex"),
                    ("virtual boy", "virtual boy"),
                    ("wii", "wii"),
                    ("wonderswan", "wonderswan"),
                    ("zx spectrum", "zx spectrum"),
                    ("zx81", "zx81"),
                ],
                max_length=200,
            ),
        ),
        migrations.AlterUniqueTogether(
            name="directories", unique_together={("device", "path")}
        ),
        migrations.AlterUniqueTogether(
            name="savefiles", unique_together={("md5", "title")}
        ),
        migrations.RemoveField(model_name="savefiles", name="directory"),
    ]
