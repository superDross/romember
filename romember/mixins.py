from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, permissions, status
from rest_framework.request import Request
from rest_framework.response import Response

from romember.utils import get_users_model


class RestrictedMixIn:
    """
    Restrict the access to users data only, unless superuser.
    """

    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self, *args, **kwargs) -> QuerySet:
        user = User.objects.get(id=self.request.user.id)
        return get_users_model(self.model, user)


class RestrictedListMixIn(RestrictedMixIn):

    filter_backends = (filters.SearchFilter, DjangoFilterBackend)

    def create(self, request: Request, *args, **kwargs) -> Response:
        """
        Override create to accept a list of objects.
        """
        serializer = self.get_serializer(
            data=request.data, many=isinstance(request.data, list)
        )
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )
