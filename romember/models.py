from django.contrib.auth.models import User
from django.db import models

from romember.storage import MediaStorage


class Device(models.Model):
    """
    A users device used to access the site.
    """

    OPERATING_SYSTEM = (
        ("linux", "l"),
        ("windows", "w"),
        ("osx", "o"),
        ("android", "a"),
        ("ios", "i"),
    )

    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    machine_id = models.CharField(max_length=200, unique=True)
    hostname = models.CharField(max_length=200)
    os = models.CharField(max_length=50, choices=OPERATING_SYSTEM)
    dist = models.CharField(max_length=100, blank=True, default=None)

    def __str__(self) -> str:
        return f"{self.owner.username} - {self.machine_id}"


class Emulator(models.Model):
    """
    Emulator save details.
    """

    CONSOLES = (
        ("2600", "2600"),
        ("32x", "32x"),
        ("3do", "3do"),
        ("5200", "5200"),
        ("7800", "7800"),
        ("all", "all"),
        ("amstrad cpc", "amstrad cpc"),
        ("arcade", "arcade"),
        ("colecovision", "colecovision"),
        ("dreamcast", "dreamcast"),
        ("famicom disk system", "famicom disk system"),
        ("flacon", "flacon"),
        ("game boy advance", "game boy advance"),
        ("game boy color", "game boy color"),
        ("game boy", "game boy"),
        ("gamecube", "gamecube"),
        ("jaguar", "jaguar"),
        ("lynx", "lynx"),
        ("mega drive", "mega drive"),
        ("nec pc-98", "nec pc-98"),
        ("neo geo", "neo geo"),
        ("nintendo 3ds", "nintendo 3ds"),
        ("nintendo 64", "nintendo 64"),
        ("nintendo ds", "nintendo ds"),
        ("nintendo entertainment system", "nintendo entertainment system"),
        ("odyssey", "odyssey"),
        ("pc-fx", "pc-fx"),
        ("playstation portable", "playstation portable"),
        ("playstation", "playstation"),
        ("super nintendo entertaiment system", "super nintendo entertaiment system"),
        ("turbograx-16", "turbograx-16"),
        ("turbograx-cd", "turbograx-cd"),
        ("vectrex", "vectrex"),
        ("virtual boy", "virtual boy"),
        ("wii", "wii"),
        ("wonderswan", "wonderswan"),
        ("zx spectrum", "zx spectrum"),
        ("zx81", "zx81"),
    )

    name = models.CharField(max_length=100, unique=True)
    save_ext = models.CharField(max_length=3)
    console = models.CharField(max_length=200, choices=CONSOLES)

    def __str__(self) -> str:
        return self.name


class Directories(models.Model):
    """
    Directory to scan for save files.
    """

    device = models.ForeignKey(Device, on_delete=models.SET_NULL, null=True)
    emulator = models.ForeignKey(Emulator, on_delete=models.SET_NULL, null=True)
    path = models.CharField(max_length=300)

    class Meta:
        unique_together = ["device", "path"]


class SaveFiles(models.Model):
    """
    A users save file.
    """

    # NOTE: required as the relationship to the file storage is dependant upon the username
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    title = models.CharField(max_length=100, null=False)
    file = models.FileField(storage=MediaStorage())
    directories = models.ManyToManyField(Directories)
    md5 = models.CharField(max_length=100)
    upload_time = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(null=False, default=False)

    class Meta:
        unique_together = ["owner", "title"]

    #     @property
    #     def upload_time(self):
    #         return self.file.file.obj.last_modified

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # NOTE: this does not work without owner as an FK as the many-to-many
        #       fields only are set after the save method is called; you can only
        #       add to M2M field after both the directories and savefile
        #       objects have been created. So the owner cannot be gathered from
        #       the directories relationship for the below purpose.
        self.file.storage.location = f"{self.owner.username}_{self.owner.pk}"


class SaveConversion(models.Model):
    """
    Describes method to convert one emulators save format to another.
    """

    CONVERSION_METHODS = (("r", "rename"), ("n", "none"))
    from_emu = models.ManyToManyField(Emulator, related_name="starting_emulator")
    to_emu = models.ManyToManyField(Emulator, related_name="ending_emulator")
    method = models.CharField(max_length=20, choices=CONVERSION_METHODS)
