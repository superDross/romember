import traceback

from django.contrib.auth.models import User
from rest_framework import serializers

from romember.fields import CustomHyperLinkedRelatedField, SaveFileDownloadField
from romember.models import Device, Directories, Emulator, SaveFiles
from romember.validators import (
    validate_file_extension,
    validate_file_integrity,
    validate_file_size,
    validate_file_type,
    validate_user_title_unique,
)


class DirSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(
        source="device.owner",
        view_name="user-detail",
        read_only=True,
        lookup_field="username",
        many=False,
    )
    device = CustomHyperLinkedRelatedField(
        model=Device,
        view_name="device-detail",
        format="html",
        required=True,
        allow_null=False,
        lookup_field="machine_id",
    )
    emulator = serializers.SlugRelatedField(
        required=True,
        slug_field="name",
        allow_null=False,
        queryset=Emulator.objects.all(),
    )
    path = serializers.CharField(required=True)
    savefiles_set = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="savefile-detail"
    )

    class Meta:
        model = Directories
        fields = [
            "id",
            "device",
            "emulator",
            "path",
            "savefiles_set",
            "device",
            "owner",
        ]


class DeviceSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(
        many=False, read_only=True, view_name="user-detail", lookup_field="username"
    )
    directories_set = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="dirs-detail"
    )

    class Meta:
        model = Device
        fields = [
            "id",
            "owner",
            "machine_id",
            "hostname",
            "os",
            "dist",
            "directories_set",
        ]
        extra_kwargs = {"url": {"lookup_field": "machine_id"}}


class SaveFileSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(
        many=False, read_only=True, view_name="user-detail", lookup_field="username"
    )
    directories = CustomHyperLinkedRelatedField(
        model=Directories,
        view_name="dirs-detail",
        format="html",
        required=True,
        allow_null=False,
        many=True,
    )
    file = serializers.FileField(use_url=False)
    upload_time = serializers.ReadOnlyField()
    download_time = serializers.ReadOnlyField()
    url = serializers.HyperlinkedIdentityField(
        view_name="savefile-detail", lookup_field="pk"
    )
    download_url = serializers.HyperlinkedIdentityField(
        view_name="savefile-download", lookup_field="pk"
    )

    class Meta:
        model = SaveFiles
        fields = [
            "id",
            "owner",
            "title",
            "file",
            "directories",
            "md5",
            "upload_time",
            "download_time",
            "deleted",
            "url",
            "download_url",
        ]

    def update(self, instance: SaveFiles, validated_data: dict) -> None:
        # I'm not sure we want this behaviour. If a file is saved again
        # then the md5 will change
        if "md5" in validated_data:
            raise serializers.ValidationError({"md5": "this field cannot be modified."})
        return super().update(instance, validated_data)

    def validate(self, data: dict) -> dict:
        if data.get("file"):
            validate_file_extension(data)
            validate_file_size(data)
            validate_user_title_unique(data, self.context["request"].user)
            validate_file_type(data)
            validate_file_integrity(data)
        return data


class SaveFileDownloadSerializer(serializers.ModelSerializer):
    file = SaveFileDownloadField()
    filename = serializers.SerializerMethodField(read_only=True, source="file")

    class Meta:
        model = SaveFiles
        fields = ["file", "filename", "md5"]

    def get_filename(self, obj: SaveFiles) -> str:
        return obj.file.name


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=True)
    email = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "email",
            "first_name",
            "last_name",
            "date_joined",
            "last_login",
            "password",
        ]
        # used in the url instead of pk
        extra_kwargs = {"url": {"lookup_field": "username"}}

    @property
    def data(self) -> dict:
        """
        Ensure password key is never included in the response.
        """
        ret = super().data
        ret.pop("password")
        return ret

    def create(self, validated_data: dict) -> User:
        """
        Same as default method except it calls User models manager method
        `create_user` instead of `create` and does not handle M2M relations.
        """
        ModelClass = self.Meta.model
        try:
            instance = ModelClass._default_manager.create_user(**validated_data)
        except TypeError:
            tb = traceback.format_exc()
            msg = (
                "Got a `TypeError` when calling `%s.%s.create()`. "
                "This may be because you have a writable field on the "
                "serializer class that is not a valid argument to "
                "`%s.%s.create()`. You may need to make the field "
                "read-only, or override the %s.create() method to handle "
                "this correctly.\nOriginal exception was:\n %s"
                % (
                    ModelClass.__name__,
                    ModelClass._default_manager.name,
                    ModelClass.__name__,
                    ModelClass._default_manager.name,
                    self.__class__.__name__,
                    tb,
                )
            )
            raise TypeError(msg)
        return instance

    def update(self, instance: User, validated_data: dict) -> User:
        """
        Same as default method but uses `set_password` to update password
        and does not handle M2M relations.
        """
        for attr, value in validated_data.items():
            if attr == "password":
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance
