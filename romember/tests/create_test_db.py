"""
python manage.py runscript create_test_data.py


Used to originally populate the database with dummy data
"""

from uuid import uuid1

import psycopg2
from django.contrib.auth.models import User
from django.db.models import Q
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from romember.models import Device, Directories, Emulator, SaveConversion, SaveFiles


def create_test_database():
    try:
        con = psycopg2.connect(username="postgres", password="postgres")
        con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = con.cursor()
        cur.execute("CREATE DATABASE test_romember_db")
        cur.execute("GRANT ALL PRIVILEGES ON DATABASE test_romember_db TO postgres")
    finally:
        con.close()


def destroy_test_database():
    try:
        con = psycopg2.connect(username="postgres", password="postgres")
        con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = con.cursor()
        cur.execute("DROP DATABASE test_romember_db")
    finally:
        con.close()


def create_users():
    superuser = User.objects.create_superuser(
        username="admin", email="admin@email.com", password="admin"
    )
    zebra = User.objects.create(
        username="zebra",
        first_name="Akira",
        last_name="Toriyama",
        email="picolo@example.com",
    )
    zebra.set_password("zebra")
    ninja = User.objects.create(
        username="ninja",
        first_name="Hamish",
        last_name="StankyArse",
        email="smelly_penguin@example.com",
    )
    ninja.set_password("ninja")


def create_devices():
    devices = (
        ("zebra", "pc-zebra", "windows", "10"),
        ("zebra", "retropie", "linux", "debian"),
        ("ninja", "ubuntu-ninja", "linux", "ubuntu"),
        ("ninja", "ninja-android", "android", "9"),
    )
    for device in devices:
        name, hname, os, dist = device
        Device.objects.create(
            owner=User.objects.get(username=name),
            machine_id=uuid1(),
            hostname=hname,
            os=os,
            dist=dist,
        )


def create_emulators():
    Emulator.objects.create(name="retroarch", save_ext="srm", console="all")
    Emulator.objects.create(name="snes9x", save_ext="srm", console="snes")
    Emulator.objects.create(name="bsnes", save_ext="srm", console="snes")
    Emulator.objects.create(name="zsnes", save_ext="srm", console="snes")
    Emulator.objects.create(name="pizza boy gba", save_ext="sav", console="gba")
    Emulator.objects.create(name="mgba", save_ext="sav", console="gba")
    Emulator.objects.create(name="gpsp", save_ext="sav", console="gba")


def create_directories():
    dirs = (
        ("zebra", "pc-zebra", "retroarch", "C:\\retropie\\roms"),
        ("zebra", "retropie", "retroarch", "/home/retropie/roms/"),
        ("zebra", "pc-zebra", "mgba", "C:\\mgba\\saves"),
        ("ninja", "ubuntu-ninja", "retroarch", "/home/ninja/retroarch/roms/"),
        ("ninja", "ninja-android", "snes9x", "/storage/emulated/0/snes9x/saves/"),
    )
    for _dir in dirs:
        name, device_name, emu, path = _dir
        Directories.objects.create(
            owner=User.objects.get(username=name),
            device=Device.objects.get(hostname=device_name),
            emulator=Emulator.objects.get(name=emu),
            path=path,
        )


def create_saves():
    saves = (
        ("ninja", "/storage/emulated/0/snes9x/saves/", "Final Fantasy VI.srm", "1"),
        ("ninja", "/home/ninja/retroarch/roms/", "Crash Bandicoot.srm", "2"),
        ("zebra", "C:\\retropie\\roms", "Mega Man V.srm", "3"),
        ("zebra", "/home/retropie/roms/", "Marvel vs Capcom.iso", "4"),
        ("zebra", "C:\\mgba\\saves", "Zelda: Minish Cap.gba", "5"),
    )

    for save in saves:
        name, path, save_name, mm = save
        owner = User.objects.get(username=name)
        SaveFiles.objects.create(
            owner=owner,
            name=save_name,
            directory=Directories.objects.get(Q(owner=owner) & Q(path=path)),
            md5=mm,
        )


def create_conversions():
    ra = Emulator.objects.get(name="retroarch")
    snes = Emulator.objects.filter(console="snes")
    gba = Emulator.objects.filter(console="gba")

    retro_to_snes = SaveConversion.objects.create(method="none")
    retro_to_gba = SaveConversion.objects.create(method="rename")

    retro_to_snes.from_emu.add(ra)
    retro_to_snes.to_emu.add(*snes)
    retro_to_gba.from_emu.add(ra)
    retro_to_gba.to_emu.add(*gba)

    retro_to_snes.save()
    retro_to_gba.save()


def fill_database():
    create_users()
    create_devices()
    create_emulators()
    create_directories()
    create_saves()
    create_conversions()


# fill_database()
