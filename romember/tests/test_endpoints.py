import io
import re
from unittest import mock

from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from rest_framework.test import APIClient

from romember import views
from romember.models import Device, Directories, SaveFiles
from romember.tests.utils import create_mock_data_file, mock_romember_s3
from romember.tests.variables import SAVEFILE_BYTES, ZIP_SAVFILE_DATA

# Difference between APIClient and APIRequestFactory
# https://stackoverflow.com/questions/32136787/django-testing-rest-framework-apirequestfactory-vs-apiclient


class EndpointTestCase(TestCase):
    """
    Base test class
    """

    maxDiff = 1000
    fixtures = ["romemberdb.json"]

    def setUp(self):
        """
        Child clases will require endpoint and view attributes
        """
        self.user = User.objects.get(username="zebra")
        self.other_user = User.objects.get(username="ninja")
        self.superuser = User.objects.get(username="admin")
        self.client = APIClient()


class RootEndpointTest(EndpointTestCase):
    def setUp(self):
        super().setUp()
        self.endpoint = "/api/v1/"

    def test_get(self):
        expected = {
            "auth": "http://testserver/api/v1/token-auth/",
            "devices": "http://testserver/api/v1/devices/",
            "directories": "http://testserver/api/v1/dirs/",
            "savefiles": "http://testserver/api/v1/savefiles/",
        }
        response = self.client.get(self.endpoint).json()
        self.assertEqual(response, expected)

    def test_post(self):
        self.assertEqual(
            self.client.post(self.endpoint, {"nonsense": "i"}).status_code, 405
        )

    def test_put(self):
        self.assertEqual(
            self.client.put(self.endpoint, {"nonsense": "i"}).status_code, 405
        )

    def test_delete(self):
        self.assertEqual(
            self.client.delete(self.endpoint, {"nonsense": "i"}).status_code, 405
        )


class RegisterEndpointTest(EndpointTestCase):
    def setUp(self):
        super().setUp()
        self.view = views.RegistrationView.as_view()
        self.endpoint = "/api/v1/register/"

    def test_create_user(self):
        res = self.client.post(
            self.endpoint,
            {"username": "SomeGuy", "email": "someguy@email.com", "password": "hello"},
        )
        self.assertEqual(res.status_code, 201)
        self.assertTrue("password" not in res.content.decode("utf8"))
        someguy = User.objects.last()
        self.assertEqual(someguy.username, "SomeGuy")
        self.assertEqual(someguy.email, "someguy@email.com")
        # ensure pw is encrypted
        self.assertTrue(someguy.password.startswith("pbkdf2_sha256"))

    def test_fields_are_required(self):
        res = self.client.post(self.endpoint, {"username": "SomeGuy"})
        self.assertEqual(
            res.content.decode("utf-8"),
            '{"email":["This field is required."],"password":["This field is required."]}',
        )


class UsersDetailEndpointTest(EndpointTestCase):
    def setUp(self):
        super().setUp()
        self.view = views.UserDetail.as_view()
        self.endpoint = "/api/v1/user"
        self.user.set_password("somethingsomethingsomething")
        self.user.save()

    def test_get(self):
        expected = {
            "id": 2,
            "username": "zebra",
            "email": "picolo@example.com",
            "first_name": "Akira",
            "last_name": "Toriyama",
            "date_joined": "2019-08-22T18:01:53.934000Z",
            "last_login": None,
        }
        self.client.force_authenticate(user=self.user)
        detail_endpoint = f"{self.endpoint}/{self.user.username}/"
        response = self.client.get(detail_endpoint).json()
        self.assertEqual(response, expected)

    def test_not_allowed_to_view_others(self):
        self.client.force_authenticate(user=self.user)
        detail_endpoint = f"{self.endpoint}/{self.superuser.username}/"
        response = self.client.get(detail_endpoint)
        self.assertEqual(404, response.status_code)

    def test_useruser_get(self):
        expected = {
            "id": 6,
            "username": "admin",
            "email": "",
            "first_name": "",
            "last_name": "",
            "date_joined": "2019-08-23T15:42:56.725000Z",
            "last_login": None,
        }
        self.client.force_authenticate(user=self.superuser)
        detail_endpoint = f"{self.endpoint}/{self.superuser.username}/"
        response = self.client.get(detail_endpoint).json()
        self.assertEqual(response, expected)

    def test_superuser_allowed_to_view_others(self):
        expected = {
            "id": 2,
            "username": "zebra",
            "email": "picolo@example.com",
            "first_name": "Akira",
            "last_name": "Toriyama",
            "date_joined": "2019-08-22T18:01:53.934000Z",
            "last_login": None,
        }
        self.client.force_authenticate(user=self.superuser)
        detail_endpoint = f"{self.endpoint}/{self.user.username}/"
        response = self.client.get(detail_endpoint).json()
        self.assertEqual(response, expected)

    def test_change_password(self):
        payload = {"password": "secure_password"}
        self.client.force_authenticate(user=self.user)
        res = self.client.patch(f"{self.endpoint}/{self.user.username}/", payload)
        self.assertEqual(res.status_code, 200)
        # ensure password is being taken out of response
        self.assertTrue("password" not in res.content.decode("utf8"))


class DeviceListEndpointTest(EndpointTestCase):
    def setUp(self):
        super().setUp()
        self.view = views.DirList.as_view()
        self.endpoint = "/api/v1/devices/"

    def test_superuser_get(self):
        expected = [
            {
                "id": 1,
                "owner": "http://testserver/api/v1/user/zebra/",
                "machine_id": "ec97938a-c506-11e9-8c16-6057180bef5a",
                "hostname": "pc-zebra",
                "os": "windows",
                "dist": "10",
                "directories_set": [
                    "http://testserver/api/v1/dirs/3/",
                    "http://testserver/api/v1/dirs/1/",
                ],
            },
            {
                "id": 2,
                "owner": "http://testserver/api/v1/user/zebra/",
                "machine_id": "ec97938b-c506-11e9-8c16-6057180bef5a",
                "hostname": "retropie",
                "os": "linux",
                "dist": "debian",
                "directories_set": ["http://testserver/api/v1/dirs/2/"],
            },
            {
                "id": 3,
                "owner": "http://testserver/api/v1/user/ninja/",
                "machine_id": "ec97938c-c506-11e9-8c16-6057180bef5a",
                "hostname": "ubuntu-ninja",
                "os": "linux",
                "dist": "ubuntu",
                "directories_set": ["http://testserver/api/v1/dirs/4/"],
            },
            {
                "id": 4,
                "owner": "http://testserver/api/v1/user/ninja/",
                "machine_id": "ec97938d-c506-11e9-8c16-6057180bef5a",
                "hostname": "ninja-android",
                "os": "android",
                "dist": "9",
                "directories_set": ["http://testserver/api/v1/dirs/5/"],
            },
        ]
        self.client.force_authenticate(user=self.superuser)
        response = self.client.get(self.endpoint).json()
        self.assertEqual(response, expected)

    def test_get(self):
        expected = [
            {
                "id": 2,
                "owner": "http://testserver/api/v1/user/zebra/",
                "machine_id": "ec97938b-c506-11e9-8c16-6057180bef5a",
                "hostname": "retropie",
                "os": "linux",
                "dist": "debian",
                "directories_set": ["http://testserver/api/v1/dirs/2/"],
            },
            {
                "id": 1,
                "owner": "http://testserver/api/v1/user/zebra/",
                "machine_id": "ec97938a-c506-11e9-8c16-6057180bef5a",
                "hostname": "pc-zebra",
                "os": "windows",
                "dist": "10",
                "directories_set": [
                    "http://testserver/api/v1/dirs/3/",
                    "http://testserver/api/v1/dirs/1/",
                ],
            },
        ]
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.endpoint).json()
        self.assertEqual(
            sorted(response, key=lambda k: k["id"]),
            sorted(expected, key=lambda k: k["id"]),
        )

    def test_post_invalid_os_not_allowed(self):
        data = {
            "machine_id": "ec97938b-c506-11e9-8c16-6057180bef5a",
            "hostname": "penguin-love",
            "os": "SuperCoolOs",
            "dist": "Mintbuntu Kylin Alpine Linux",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data)
        self.assertEqual(response.status_code, 400)
        self.assertTrue(
            re.search(r"os.*is not a valid choice", response.content.decode("utf-8"))
        )

    def test_post_success(self):
        data = {
            "machine_id": "ec97938b-c506-11e9-8c16-6057180b89898",
            "hostname": "penguin-love",
            "os": "linux",
            "dist": "Mintbuntu Kylin Alpine Linux",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Device.objects.get(hostname="penguin-love"))

    def test_post_list(self):
        data = [
            {
                "machine_id": "xx97938b-c506-11e9-8c16-6057180b89898",
                "hostname": "penguin-love",
                "os": "linux",
                "dist": "Mintbuntu Kylin Alpine Linux",
            },
            {
                "machine_id": "zz97938b-c506-11e9-8c16-6057180b89898",
                "hostname": "penguin-love",
                "os": "linux",
                "dist": "Mintbuntu Kylin Alpine Linux",
            },
        ]
        # NOTE: does not work because the testing suite does not accept lists
        self.client.force_authenticate(user=self.user)
        data
        # response = self.client.post(self.endpoint, data)
        # self.assertEqual(response.status_code, 201)
        # self.assertTrue(Device.objects.get(hostname="penguin-love"))

    def test_put_not_allowed(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.endpoint, {})
        self.assertEqual(response.status_code, 405)

    def test_delete_not_allowed(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 405)


class DeviceDetailEndpointTest(EndpointTestCase):
    def setUp(self):
        super().setUp()
        self.view = views.DirDetail.as_view()
        user_device = self.user.device_set.first()
        self.user_device_endpoint = f"/api/v1/devices/{user_device.machine_id}/"
        other_user_device = Device.objects.filter(owner=self.other_user).first()
        self.other_user_device_endpoint = (
            f"/api/v1/devices/{other_user_device.machine_id}/"
        )

    def test_get(self):
        expected = {
            "directories_set": [
                "http://testserver/api/v1/dirs/3/",
                "http://testserver/api/v1/dirs/1/",
            ],
            "dist": "10",
            "hostname": "pc-zebra",
            "id": 1,
            "machine_id": "ec97938a-c506-11e9-8c16-6057180bef5a",
            "os": "windows",
            "owner": "http://testserver/api/v1/user/zebra/",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.user_device_endpoint).json()
        self.assertEqual(response, expected)

    def test_superuser_get(self):
        expected = {
            "directories_set": [
                "http://testserver/api/v1/dirs/3/",
                "http://testserver/api/v1/dirs/1/",
            ],
            "dist": "10",
            "hostname": "pc-zebra",
            "id": 1,
            "machine_id": "ec97938a-c506-11e9-8c16-6057180bef5a",
            "os": "windows",
            "owner": "http://testserver/api/v1/user/zebra/",
        }
        self.client.force_authenticate(user=self.superuser)
        response = self.client.get(self.user_device_endpoint).json()
        self.assertEqual(response, expected)

    def test_other_user_get_forbidden(self):
        """ User cannot access another users dirs detail."""
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.other_user_device_endpoint)
        self.assertEqual(response.status_code, 404)

    def test_post_not_allowed(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.user_device_endpoint, {})
        self.assertEqual(response.status_code, 405)

    def test_patch_ok(self):
        data = {"os": "ios", "dist": "1"}
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(self.user_device_endpoint, data)
        self.assertEqual(response.status_code, 200)

    def test_delete_no_content(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(self.user_device_endpoint)
        self.assertEqual(response.status_code, 204)


class DirsListEndpointTest(EndpointTestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.view = views.DirList.as_view()
        self.endpoint = "/api/v1/dirs/"

    def test_superuser_get(self):
        expected = [
            {
                "device": "http://testserver/api/v1/devices/ec97938a-c506-11e9-8c16-6057180bef5a/",
                "emulator": "retroarch",
                "id": 1,
                "owner": "http://testserver/api/v1/user/zebra/",
                "path": "C:\\retropie\\roms",
                "savefiles_set": ["http://testserver/api/v1/savefiles/3/"],
            },
            {
                "device": "http://testserver/api/v1/devices/ec97938b-c506-11e9-8c16-6057180bef5a/",
                "emulator": "retroarch",
                "id": 2,
                "owner": "http://testserver/api/v1/user/zebra/",
                "path": "/home/retropie/roms/",
                "savefiles_set": ["http://testserver/api/v1/savefiles/4/"],
            },
            {
                "device": "http://testserver/api/v1/devices/ec97938a-c506-11e9-8c16-6057180bef5a/",
                "emulator": "mgba",
                "id": 3,
                "owner": "http://testserver/api/v1/user/zebra/",
                "path": "C:\\mgba\\saves",
                "savefiles_set": [
                    "http://testserver/api/v1/savefiles/3/",
                    "http://testserver/api/v1/savefiles/5/",
                ],
            },
            {
                "device": "http://testserver/api/v1/devices/ec97938c-c506-11e9-8c16-6057180bef5a/",
                "emulator": "retroarch",
                "id": 4,
                "owner": "http://testserver/api/v1/user/ninja/",
                "path": "/home/ninja/retroarch/roms/",
                "savefiles_set": ["http://testserver/api/v1/savefiles/2/"],
            },
            {
                "device": "http://testserver/api/v1/devices/ec97938d-c506-11e9-8c16-6057180bef5a/",
                "emulator": "snes9x",
                "id": 5,
                "owner": "http://testserver/api/v1/user/ninja/",
                "path": "/storage/emulated/0/snes9x/saves/",
                "savefiles_set": ["http://testserver/api/v1/savefiles/1/"],
            },
        ]

        self.client.force_authenticate(user=self.superuser)
        response = self.client.get(self.endpoint).json()
        self.assertEqual(response, expected)

    def test_get(self):
        expected = [
            {
                "device": "http://testserver/api/v1/devices/ec97938a-c506-11e9-8c16-6057180bef5a/",
                "emulator": "mgba",
                "id": 3,
                "owner": "http://testserver/api/v1/user/zebra/",
                "path": "C:\\mgba\\saves",
                "savefiles_set": [
                    "http://testserver/api/v1/savefiles/3/",
                    "http://testserver/api/v1/savefiles/5/",
                ],
            },
            {
                "device": "http://testserver/api/v1/devices/ec97938b-c506-11e9-8c16-6057180bef5a/",
                "emulator": "retroarch",
                "id": 2,
                "owner": "http://testserver/api/v1/user/zebra/",
                "path": "/home/retropie/roms/",
                "savefiles_set": ["http://testserver/api/v1/savefiles/4/"],
            },
            {
                "device": "http://testserver/api/v1/devices/ec97938a-c506-11e9-8c16-6057180bef5a/",
                "emulator": "retroarch",
                "id": 1,
                "owner": "http://testserver/api/v1/user/zebra/",
                "path": "C:\\retropie\\roms",
                "savefiles_set": ["http://testserver/api/v1/savefiles/3/"],
            },
        ]
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.endpoint).json()
        self.assertEqual(
            sorted(response, key=lambda k: k["id"]),
            sorted(expected, key=lambda k: k["id"]),
        )

    def test_post_created(self):
        data = {
            "device": "http://127.0.0.1:8000/api/v1/devices/ec97938a-c506-11e9-8c16-6057180bef5a/",
            "emulator": "mgba",
            "path": "/no/where/",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Directories.objects.get(path="/no/where/"))

    def test_post_fails_if_object_exists(self):
        existing_dir = Directories.objects.filter(device__owner=self.user).first()
        data = {
            "device": f"http://127.0.0.1:8000/api/v1/devices/{existing_dir.device.machine_id}/",
            "emulator": "mgba",
            "path": existing_dir.path,
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            b'{"non_field_errors":["The fields device, path must make a unique set."]}',
            response.content,
        )

    def test_patch_not_allowed(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(self.endpoint, {})
        self.assertEqual(response.status_code, 405)

    def test_put(self):
        # TODO: should be used for bulk update
        pass

    def test_delete_not_allowed(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(self.endpoint)
        self.assertEqual(response.status_code, 405)


class DirsDetailEndpointTest(EndpointTestCase):
    def setUp(self):
        super().setUp()
        self.view = views.DirDetail.as_view()
        user_dir = Directories.objects.filter(device__owner=self.user).first()
        self.user_dir_endpoint = f"/api/v1/dirs/{user_dir.id}/"
        other_user_dir = Directories.objects.filter(
            device__owner=self.other_user
        ).first()
        self.other_user_dir_endpoint = f"/api/v1/dirs/{other_user_dir.id}/"

    def test_get(self):
        """ User can access their own dirs detail."""
        expected = {
            "id": 1,
            "owner": "http://testserver/api/v1/user/zebra/",
            "device": "http://testserver/api/v1/devices/ec97938a-c506-11e9-8c16-6057180bef5a/",
            "emulator": "retroarch",
            "path": "C:\\retropie\\roms",
            "savefiles_set": ["http://testserver/api/v1/savefiles/3/"],
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.user_dir_endpoint).json()
        self.assertEqual(response, expected)

    def test_superuser_get(self):
        """ Superuser can access anyones dirs detail."""
        expected = {
            "id": 1,
            "owner": "http://testserver/api/v1/user/zebra/",
            "device": "http://testserver/api/v1/devices/ec97938a-c506-11e9-8c16-6057180bef5a/",
            "emulator": "retroarch",
            "path": "C:\\retropie\\roms",
            "savefiles_set": ["http://testserver/api/v1/savefiles/3/"],
        }
        self.client.force_authenticate(user=self.superuser)
        response = self.client.get(self.user_dir_endpoint).json()
        self.assertEqual(response, expected)

    def test_other_user_dir_get(self):
        """ User cannot access another users dirs detail."""
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.other_user_dir_endpoint)
        self.assertEqual(response.status_code, 404)

    def test_post_not_allowed(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.user_dir_endpoint, {})
        self.assertEqual(response.status_code, 405)

    def test_put(self):
        data = {"path": "/some/new/place"}
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(self.user_dir_endpoint, data)
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(self.user_dir_endpoint)
        self.assertEqual(response.status_code, 204)


class SaveFilesListEndpointTest(EndpointTestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        self.view = views.SaveFileList.as_view()
        self.endpoint = "/api/v1/savefiles/"

    def test_superuser_get(self):
        expected = [
            {
                "deleted": False,
                "directories": ["http://testserver/api/v1/dirs/5/"],
                "download_url": "http://testserver/api/v1/savefiles/1/download/",
                "file": "Final Fantasy VI.srm",
                "id": 1,
                "md5": "1",
                "owner": "http://testserver/api/v1/user/ninja/",
                "title": "Final Fantasy VI",
                "upload_time": "2019-08-22T18:01:54.208000Z",
                "url": "http://testserver/api/v1/savefiles/1/",
            },
            {
                "deleted": False,
                "directories": ["http://testserver/api/v1/dirs/4/"],
                "download_url": "http://testserver/api/v1/savefiles/2/download/",
                "file": "Crash Bandicoot.srm",
                "id": 2,
                "md5": "2",
                "owner": "http://testserver/api/v1/user/ninja/",
                "title": "Crash Bandicoot",
                "upload_time": "2019-08-22T18:01:54.214000Z",
                "url": "http://testserver/api/v1/savefiles/2/",
            },
            {
                "deleted": False,
                "directories": [
                    "http://testserver/api/v1/dirs/1/",
                    "http://testserver/api/v1/dirs/3/",
                ],
                "download_url": "http://testserver/api/v1/savefiles/3/download/",
                "file": "Mega Man V.srm",
                "id": 3,
                "md5": "3",
                "owner": "http://testserver/api/v1/user/zebra/",
                "title": "Mega Man V",
                "upload_time": "2019-08-22T18:01:54.220000Z",
                "url": "http://testserver/api/v1/savefiles/3/",
            },
            {
                "deleted": False,
                "directories": ["http://testserver/api/v1/dirs/2/"],
                "download_url": "http://testserver/api/v1/savefiles/4/download/",
                "file": "Marvel vs Capcom.srm",
                "id": 4,
                "md5": "4",
                "owner": "http://testserver/api/v1/user/zebra/",
                "title": "Marvel vs Capcom",
                "upload_time": "2019-08-22T18:01:54.225000Z",
                "url": "http://testserver/api/v1/savefiles/4/",
            },
            {
                "deleted": False,
                "directories": ["http://testserver/api/v1/dirs/3/"],
                "download_url": "http://testserver/api/v1/savefiles/5/download/",
                "file": "Zelda: Minish Cap.sav",
                "id": 5,
                "md5": "5",
                "owner": "http://testserver/api/v1/user/zebra/",
                "title": "Zelda: Minish Cap",
                "upload_time": "2019-08-22T18:01:54.232000Z",
                "url": "http://testserver/api/v1/savefiles/5/",
            },
        ]
        self.client.force_authenticate(user=self.superuser)
        response = self.client.get(self.endpoint).json()
        self.assertEqual(response, expected)

    def test_get(self):
        expected = [
            {
                "deleted": False,
                "directories": ["http://testserver/api/v1/dirs/3/"],
                "download_url": "http://testserver/api/v1/savefiles/5/download/",
                "file": "Zelda: Minish Cap.sav",
                "id": 5,
                "md5": "5",
                "owner": "http://testserver/api/v1/user/zebra/",
                "title": "Zelda: Minish Cap",
                "upload_time": "2019-08-22T18:01:54.232000Z",
                "url": "http://testserver/api/v1/savefiles/5/",
            },
            {
                "deleted": False,
                "directories": ["http://testserver/api/v1/dirs/2/"],
                "download_url": "http://testserver/api/v1/savefiles/4/download/",
                "file": "Marvel vs Capcom.srm",
                "id": 4,
                "md5": "4",
                "owner": "http://testserver/api/v1/user/zebra/",
                "title": "Marvel vs Capcom",
                "upload_time": "2019-08-22T18:01:54.225000Z",
                "url": "http://testserver/api/v1/savefiles/4/",
            },
            {
                "deleted": False,
                "directories": [
                    "http://testserver/api/v1/dirs/1/",
                    "http://testserver/api/v1/dirs/3/",
                ],
                "download_url": "http://testserver/api/v1/savefiles/3/download/",
                "file": "Mega Man V.srm",
                "id": 3,
                "md5": "3",
                "owner": "http://testserver/api/v1/user/zebra/",
                "title": "Mega Man V",
                "upload_time": "2019-08-22T18:01:54.220000Z",
                "url": "http://testserver/api/v1/savefiles/3/",
            },
        ]
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.endpoint).json()
        self.assertEqual(
            sorted(response, key=lambda k: k["id"]),
            sorted(expected, key=lambda k: k["id"]),
        )

    @mock_romember_s3
    def test_post_corrupt_file_does_not_validate(self):
        # file_data = open(f'{BASE_DIR}/romember/tests/files/savefile.srm', 'rb')
        file_mock = create_mock_data_file(
            wraps=io.BytesIO(SAVEFILE_BYTES),
            filename="mea.srm",
            name="Mega Man VII.srm",
        )
        data = {
            "deleted": False,
            "directories": ["http://testserver/api/v1/dirs/1/"],
            "file": file_mock.file,
            "md5": "fakethingymd5",
            "title": "Mega Man VII",
            "upload_time": "2019-08-22T18:01:54.220000Z",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data, format="multipart")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content.decode("utf-8"),
            '{"file":["file is corrupted; failed md5 checksum"]}',
        )

    @mock_romember_s3
    def test_post_binary_file(self):
        # file_data = open(f'{BASE_DIR}/romember/tests/files/savefile.srm', 'rb')
        file_mock = create_mock_data_file(
            wraps=io.BytesIO(SAVEFILE_BYTES),
            filename="mea.srm",
            name="Mega Man VII.srm",
        )
        data = {
            "deleted": False,
            "directories": ["http://testserver/api/v1/dirs/1/"],
            "file": file_mock.file,
            "md5": "41408db588117af366f1d813518758ad",
            "title": "Mega Man VII",
            "upload_time": "2019-08-22T18:01:54.220000Z",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data, format="multipart")
        self.assertEqual(response.status_code, 201)
        sf = SaveFiles.objects.last()
        self.assertEqual(sf.file.name, "Mega_Man_VII.srm")
        # ensure it is saved in the correct directory
        self.assertEqual(sf.file.storage.location, f"{sf.owner.username}_{sf.owner.pk}")

    @mock_romember_s3
    def test_post_zip_file(self):
        # file_data = open(f'{BASE_DIR}/romember/tests/files/savefile.zip', 'rb')
        file_mock = create_mock_data_file(
            wraps=io.BytesIO(ZIP_SAVFILE_DATA),
            filename="mea.zip",
            name="Mega Man VII.zip",
        )
        data = {
            "deleted": False,
            "directories": ["http://testserver/api/v1/dirs/1/"],
            "file": file_mock.file,
            "md5": "afdde9521d12f914a81b5b34f4e2c129",
            "title": "Mega Man VII",
            "upload_time": "2019-08-22T18:01:54.220000Z",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data, format="multipart")
        self.assertEqual(response.status_code, 201)
        sf = SaveFiles.objects.last()
        self.assertEqual(sf.file.name, "Mega_Man_VII.zip")
        # ensure it is saved in the correct directory
        self.assertEqual(sf.file.storage.location, f"{sf.owner.username}_{sf.owner.pk}")

    @mock_romember_s3
    def test_post_invalid_filetype(self):
        # file_data = open(f'{BASE_DIR}/romember/tests/files/savefile.srm', 'rb')
        file_mock = create_mock_data_file(
            wraps=io.StringIO("hello"), filename="hello.srm", name="hello.srm"
        )
        data = {
            "deleted": False,
            "directories": ["http://testserver/api/v1/dirs/1/"],
            "file": file_mock.file,
            "md5": "3",
            "title": "Super Fake File",
            "upload_time": "2019-08-22T18:01:54.220000Z",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data, format="multipart")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content.decode("utf-8"),
            '{"file":["hello.srm does not contain binary data"]}',
        )

    @mock_romember_s3
    def test_post_wrong_file_extension(self):
        file_mock = create_mock_data_file(
            wraps=io.StringIO("hello"), filename="hello.txt", name="hello.txt"
        )
        data = {
            "deleted": False,
            "directories": ["http://testserver/api/v1/dirs/1/"],
            "file": file_mock.file,
            "md5": "3",
            "title": "Super Fake File",
            "upload_time": "2019-08-22T18:01:54.220000Z",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data, format="multipart")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content.decode("utf-8"),
            '{"file":["hello.txt extension is invalid"]}',
        )

    @mock_romember_s3
    def test_post_large_file_bad_request(self):
        file_mock = create_mock_data_file(
            wraps=io.BytesIO(b"x" * 400000000),
            filename="mea.srm",
            name="Mega Man VII.srm",
        )
        data = {
            "deleted": False,
            "directories": ["http://testserver/api/v1/dirs/1/"],
            "file": file_mock.file,
            "md5": "41408db588117af366f1d813518758ad",
            "title": "Mega Man VII",
            "upload_time": "2019-08-22T18:01:54.220000Z",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data, format="multipart")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content.decode("utf-8"),
            '{"file":["Mega Man VII.srm is larger than 2mb"]}',
        )

    @mock_romember_s3
    def test_post_file_with_other_users_dirs(self):
        file_mock = create_mock_data_file(
            wraps=io.BytesIO(SAVEFILE_BYTES), filename="mea.srm", name="Mega Man V.srm"
        )
        data = {
            "deleted": False,
            "directories": ["http://testserver/api/v1/dirs/4/"],
            "file": file_mock.file,
            "md5": "3",
            "title": "Mega Man V",
            "upload_time": "2019-08-22T18:01:54.220000Z",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data, format="multipart")
        self.assertEqual(response.status_code, 400)

    @mock_romember_s3
    def test_post_file_already_present_bad_request(self):
        file_mock = create_mock_data_file(
            wraps=io.BytesIO(SAVEFILE_BYTES), filename="mea.srm", name="Mega Man V.srm"
        )
        data = {
            "deleted": False,
            "directories": ["http://testserver/api/v1/dirs/1/"],
            "file": file_mock.file,
            "md5": "3",
            "title": "Mega Man V",
            "upload_time": "2019-08-22T18:01:54.220000Z",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.endpoint, data, format="multipart")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.content.decode("utf-8"),
            '{"non_field_errors":["The fields owner, title must make a unique set."]}',
        )


class SaveFileDetailEndpointTest(EndpointTestCase):
    def setUp(self):
        super().setUp()
        self.view = views.SaveFileDetail.as_view()
        user_savefile = SaveFiles.objects.filter(owner=self.user).first()
        self.user_savefile_endpoint = f"/api/v1/savefiles/{user_savefile.id}/"
        other_user_savefile = SaveFiles.objects.filter(owner=self.other_user).first()
        self.other_user_savefile_endpoint = (
            f"/api/v1/savefiles/{other_user_savefile.id}/"
        )

    def test_get(self):
        expected = {
            "deleted": False,
            "directories": [
                "http://testserver/api/v1/dirs/1/",
                "http://testserver/api/v1/dirs/3/",
            ],
            "download_url": "http://testserver/api/v1/savefiles/3/download/",
            "file": "Mega Man V.srm",
            "id": 3,
            "md5": "3",
            "owner": "http://testserver/api/v1/user/zebra/",
            "title": "Mega Man V",
            "upload_time": "2019-08-22T18:01:54.220000Z",
            "url": "http://testserver/api/v1/savefiles/3/",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.user_savefile_endpoint).json()
        self.assertEqual(response, expected)

    def test_superuser_get(self):
        expected = {
            "deleted": False,
            "directories": [
                "http://testserver/api/v1/dirs/1/",
                "http://testserver/api/v1/dirs/3/",
            ],
            "download_url": "http://testserver/api/v1/savefiles/3/download/",
            "file": "Mega Man V.srm",
            "id": 3,
            "md5": "3",
            "owner": "http://testserver/api/v1/user/zebra/",
            "title": "Mega Man V",
            "upload_time": "2019-08-22T18:01:54.220000Z",
            "url": "http://testserver/api/v1/savefiles/3/",
        }
        self.client.force_authenticate(user=self.superuser)
        response = self.client.get(self.user_savefile_endpoint).json()
        self.assertEqual(response, expected)

    def test_other_user_get(self):
        """ User cannot access another users dirs detail."""
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.other_user_savefile_endpoint)
        self.assertEqual(response.status_code, 404)

    def test_post(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(self.user_savefile_endpoint, {})
        self.assertEqual(response.status_code, 405)

    def test_put(self):
        data = {"path": "/some/new/place"}
        self.client.force_authenticate(user=self.user)
        response = self.client.patch(self.user_savefile_endpoint, data)
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.delete(self.user_savefile_endpoint)
        self.assertEqual(response.status_code, 204)


class SaveFileDownloadEndpointTest(EndpointTestCase):
    def setUp(self):
        super().setUp()
        self.view = views.SaveFileDetail.as_view()
        self.endpoint = "/api/v1/savefiles"

    @mock_romember_s3
    def test_get(self):
        # add file to mock s3
        sf = SaveFiles(
            file=SimpleUploadedFile("Super_Tux.srm", b"this that whatever"),
            title="Super Tux",
            md5="hgjgjg",
            owner=self.user,
        )
        sf.save()
        sf.directories.add(Directories.objects.get(id=1))
        self.client.force_authenticate(user=self.user)
        savefile = SaveFiles.objects.get(title="Super Tux")
        savefile_endpoint = f"{self.endpoint}/{savefile.id}/download/"
        response = self.client.get(savefile_endpoint).json()
        base_url = "https://romember-savefiles.s3.amazonaws.com/zebra_2/Super_Tux.srm"
        self.assertTrue(response["file"].startswith(base_url))
        self.assertEqual(response["filename"], "Super_Tux.srm")
        self.assertEqual(response["md5"], "hgjgjg")
