from django.contrib.auth.models import User
from django.test import TestCase

from romember.models import Device, Directories, Emulator, SaveConversion, SaveFiles


class DeviceModelTest(TestCase):
    fixtures = ["romemberdb.json"]

    def setUp(self):
        self.dev = Device.objects.first()

    def test_owner_type(self):
        self.assertTrue(isinstance(self.dev.owner, User))

    def test_str_method(self):
        self.assertEqual(
            str(self.dev), f"{self.dev.owner.username} - {self.dev.machine_id}"
        )

    def test_machine_id(self):
        self.assertEqual(len(self.dev.machine_id), 36)
        self.assertEqual(self.dev.machine_id.count("-"), 4)


class EmulatorModelTest(TestCase):
    fixtures = ["romemberdb.json"]

    def setUp(self):
        self.emu = Emulator.objects.first()

    def test_str_method(self):
        self.assertEqual(str(self.emu), "retroarch")

    def test_save_ext_max_length(self):
        self.assertEqual(self.emu._meta.get_field("save_ext").max_length, 3)


class DirectoryModelTest(TestCase):
    fixtures = ["romemberdb.json"]

    def setUp(self):
        self.direct = Directories.objects.first()

    def test_owner_type(self):
        self.assertTrue(isinstance(self.direct.device.owner, User))

    def test_device_type(self):
        self.assertTrue(isinstance(self.direct.device, Device))

    def test_emulator_type(self):
        self.assertTrue(isinstance(self.direct.emulator, Emulator))


class SaveFilesModelTest(TestCase):
    fixtures = ["romemberdb.json"]

    def setUp(self):
        self.savefile = SaveFiles.objects.first()

    def test_owner_type(self):
        self.assertTrue(isinstance(self.savefile.owner, User))

    def test_directory_type(self):
        self.assertTrue(isinstance(self.savefile.directories.first(), Directories))


class SaveConversionTest(TestCase):
    fixtures = ["romemberdb.json"]

    def setUp(self):
        self.convert = SaveConversion.objects.first()

    def test_from_emu_type(self):
        self.assertTrue(isinstance(self.convert.from_emu.first(), Emulator))

    def test_to_emu_type(self):
        self.assertTrue(isinstance(self.convert.to_emu.first(), Emulator))
