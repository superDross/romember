from django.contrib.auth.models import User
from django.test import TestCase

from romember.models import Device, Emulator
from romember.utils import get_users_model


class TestGetUserModels(TestCase):
    fixtures = ["romemberdb.json"]

    def setUp(self):
        self.user = User.objects.first()
        self.superuser = User.objects.filter(is_superuser=True)[0]

    def test_superuser(self):
        objects = get_users_model(Device, self.superuser)
        self.assertEqual(len(objects), len(Device.objects.all()))

    def test_user_models(self):
        objects = get_users_model(Device, self.user)
        self.assertEqual(len(objects), len(Device.objects.filter(owner=self.user)))

    def test_invalid_model(self):
        self.assertRaises(ValueError, get_users_model, model=Emulator, user=self.user)
