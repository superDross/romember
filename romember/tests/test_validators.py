import io
from unittest import mock

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.exceptions import ValidationError

from romember.tests.utils import create_mock_data_file
from romember.tests.variables import SAVEFILE_BYTES, ZIP_SAVFILE_DATA, ZIP_STRING_DATA
from romember.validators import (
    validate_file_extension,
    validate_file_integrity,
    validate_file_size,
    validate_file_type,
    validate_user_title_unique,
)


class TestSaveFilesSerializersValidators(TestCase):
    fixtures = ["romemberdb.json"]

    def test_validate_user_title_unique(self):
        user = User.objects.get(username="zebra")
        data = {"title": "Mega Man V"}
        with self.assertRaises(ValidationError):
            validate_user_title_unique(data, user)

    def test_validate_accepted_file_types(self):
        file_mock = create_mock_data_file(wraps=io.BytesIO(SAVEFILE_BYTES))
        data_mock = {"file": file_mock}
        validation = validate_file_type(data_mock)
        self.assertEqual(validation, data_mock)

    def test_validate_text_file_type(self):
        file_mock = create_mock_data_file(
            wraps=io.StringIO("This is a super cool text file")
        )
        data_mock = {"file": file_mock}
        with self.assertRaises(ValidationError):
            validate_file_type(data_mock)

    def test_validate_file_integrity(self):
        file_mock = create_mock_data_file(
            wraps=io.BytesIO(b"This is a super cool text file")
        )
        data_mock = {"file": file_mock, "md5": "not real md5"}
        with self.assertRaises(ValidationError):
            validate_file_integrity(data_mock)

    def test_validate_accepted_zip_file_type(self):
        file_mock = create_mock_data_file(wraps=io.BytesIO(ZIP_SAVFILE_DATA))
        data_mock = {"file": file_mock}
        validation = validate_file_type(data_mock)
        self.assertEqual(validation, data_mock)

    def test_validate_unaccepted_zip_file_type(self):
        file_mock = create_mock_data_file(wraps=io.BytesIO(ZIP_STRING_DATA))
        with self.assertRaises(ValidationError):
            validate_file_type({"file": file_mock})

    def test_size_too_big(self):
        file_mock = mock.Mock()
        file_mock.size = 4000000000
        with self.assertRaises(ValidationError):
            validate_file_size({"file": file_mock})

    def test_wrong_file_type(self):
        file_mock = mock.Mock()
        file_mock.name = "test.txt"
        with self.assertRaises(ValidationError):
            validate_file_extension({"file": file_mock})
