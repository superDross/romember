from unittest import mock

import boto3
from moto import mock_s3


@mock.patch("builtins.open", mock.mock_open())
def create_mock_data_file(wraps, name="default", filename="default.srm", kwargs={}):
    """
    Creates a mock file for testing. kwargs add attributes to mock.file
    """
    file_mock = mock.Mock()
    file_mock.name = name
    file_mock.file = mock.MagicMock(spec=open(filename), wraps=wraps)
    file_mock.file.name = name
    for k, i in kwargs.items():
        setattr(file_mock.file, k, i)
    return file_mock


def mock_romember_s3(func):
    """
    Decorator that uses romember AWS credentials with mock_s3 decorator
    """

    @mock_s3
    def get_mocked_bucket(*args, **kwargs):
        """
        Creates mock bucket to save files to
        """
        conn = boto3.resource("s3", region_name="us-east-1")
        conn.create_bucket(Bucket="romember-savefiles")
        func(*args, **kwargs)

    return get_mocked_bucket
