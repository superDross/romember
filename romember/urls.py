from django.urls import include, path
from rest_framework.authtoken.views import obtain_auth_token

from romember import views

urlpatterns = [
    path("", views.redirect_root),
    path("api/v1/", views.api_root),
    path("api/v1/auth/", include("rest_framework.urls")),
    path("api/v1/token-auth/", obtain_auth_token, name="api_token_auth"),
    path("api/v1/register/", views.RegistrationView.as_view(), name="register"),
    path(
        "api/v1/user/<slug:username>/", views.UserDetail.as_view(), name="user-detail"
    ),
    path("api/v1/dirs/", views.DirList.as_view(), name="dirs-list"),
    # get unique by using the device__machine_id & path fields
    path("api/v1/dirs/<int:pk>/", views.DirDetail.as_view(), name="dirs-detail"),
    path("api/v1/devices/", views.DeviceList.as_view(), name="device-list"),
    path(
        "api/v1/devices/<slug:machine_id>/",
        views.DeviceDetail.as_view(),
        name="device-detail",
    ),
    path("api/v1/savefiles/", views.SaveFileList.as_view(), name="savefile-list"),
    # get unique by using the name & user__username fields
    path(
        "api/v1/savefiles/<int:pk>/",
        views.SaveFileDetail.as_view(),
        name="savefile-detail",
    ),
    path(
        "api/v1/savefiles/<int:pk>/download/",
        views.SaveFileDownload.as_view(),
        name="savefile-download",
    ),
]
