from django.contrib.auth.models import User
from django.db.models import Model
from django.db.models.query import QuerySet

from romember.models import Device, Directories, SaveFiles


def user_dir_path(user: User, filename: str) -> str:
    return f"{user.username}_{user.id}/{filename}"


def get_users_model(model: Model, user: User) -> QuerySet:
    """
    Returns objects owned by the given user.
    """
    if user.is_superuser:
        return model.objects.all()
    elif model in [Device, SaveFiles]:
        return model.objects.filter(owner=user)
    elif model == Directories:
        return model.objects.filter(device__owner=user)
    else:
        raise ValueError(f"Model {model} can not be filtered for an owner")
