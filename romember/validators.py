import hashlib
import io
import zipfile

import magic
from django.contrib.auth.models import User
from rest_framework import serializers

from romember.models import Emulator, SaveFiles


def validate_file_integrity(data: dict) -> None:
    """
    Ensure the file has not corrupted.
    """
    f = data.get("file").file
    file_contents = f.read(1024)
    f.seek(0)
    hashed = hashlib.md5(file_contents)
    checksum = hashed.hexdigest()
    if checksum != data["md5"]:
        raise serializers.ValidationError(
            {"file": "file is corrupted; failed md5 checksum"}
        )


def validate_user_title_unique(data: dict, user: User) -> dict:
    """
    The owner is not sent with the POST request but rather saved to the
    object at the view level. So assigning a UniqueTogether validator
    on the serializer will not work. Instead a psycopg2 error will occur
    as the unique together constraints will fail at the view level.

    This is a work around to ensure the user recieves a 400 error
    response.
    """
    qs = SaveFiles.objects.filter(owner=user, title=data["title"])
    if qs:
        raise serializers.ValidationError(
            {"non_field_errors": ["The fields owner, title must make a unique set."]}
        )
    return data


def validate_file_size(data: dict) -> None:
    """
    Ensure file is below 2Mb.
    """
    if data["file"].size > 2000000:
        raise serializers.ValidationError(
            {"file": f"{data['file'].name} is larger than 2mb"}
        )


def validate_file_type(data: dict) -> dict:
    """
    Ensure file is or contains a binary file.
    """
    error = serializers.ValidationError(
        {"file": f"{data['file'].name} does not contain binary data"}
    )
    f = data.get("file").file
    file_contents = f.read(1024)
    f.seek(0)
    file_type = magic.from_buffer(file_contents, mime=True)
    if file_type == "application/octet-stream":
        return data
    elif file_type == "application/zip":
        unzipped = zipfile.ZipFile(io.BytesIO(file_contents))
        # TODO: delete all namelist files
        for file_name in unzipped.namelist():
            unzipped_file_type = magic.from_buffer(unzipped.read(file_name), mime=True)
            if unzipped_file_type != "application/octet-stream":
                raise error
            return data

    else:
        raise error


def validate_file_extension(data: dict) -> None:
    """
    Only let file extension present in Emulator objects.
    """
    valid_exts = set([x.save_ext for x in Emulator.objects.all()] + ["zip"])
    ext = data.get("file").name.split(".")[-1]
    if ext not in valid_exts:
        raise serializers.ValidationError(
            {"file": f"{data['file'].name} extension is invalid"}
        )
