from django.contrib.auth.models import User
from django.db.models.query import QuerySet
from django.http import HttpResponseRedirect

from rest_framework import generics, permissions, status
from rest_framework.decorators import api_view
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.reverse import reverse

from romember.mixins import RestrictedListMixIn, RestrictedMixIn
from romember.models import Device, Directories, SaveFiles
from romember.serializers import (
    DeviceSerializer,
    DirSerializer,
    SaveFileDownloadSerializer,
    SaveFileSerializer,
    UserSerializer,
)


def redirect_root(request: Request) -> HttpResponseRedirect:
    return HttpResponseRedirect("/api/v1/")


@api_view(["GET"])
def api_root(request: Request, format=None) -> Response:
    return Response(
        {
            "auth": reverse("api_token_auth", request=request, format=format),
            "devices": reverse("device-list", request=request, format=format),
            "directories": reverse("dirs-list", request=request, format=format),
            "savefiles": reverse("savefile-list", request=request, format=format),
        }
    )


class RegistrationView(generics.CreateAPIView):
    serializer_class = UserSerializer


class SaveFileDetail(RestrictedMixIn, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = SaveFileSerializer
    model = SaveFiles

    def destroy(self, request: Request, *args, **kwargs) -> Response:
        """
        Don't delete the file, just mark it as deleted
        """
        instance = self.get_object()
        instance.deleted = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class SaveFileDownload(RestrictedMixIn, generics.RetrieveAPIView):
    model = SaveFiles
    serializer_class = SaveFileDownloadSerializer


class SaveFileList(RestrictedListMixIn, generics.ListCreateAPIView):
    serializer_class = SaveFileSerializer
    model = SaveFiles

    filterset_fields = ["title", "deleted", "owner__username"]
    search_fields = ["title"]
    # multipart form instead of JSON
    parser_classes = (MultiPartParser, FormParser)

    def perform_create(self, serializer: SaveFileSerializer) -> SaveFileSerializer:
        serializer.save(owner=self.request.user)


class DeviceDetail(RestrictedMixIn, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DeviceSerializer
    model = Device
    # can be used in url config
    lookup_field = "machine_id"


class DeviceList(RestrictedListMixIn, generics.ListCreateAPIView):
    serializer_class = DeviceSerializer
    model = Device

    filterset_fields = ["hostname", "os", "dist", "machine_id"]
    search_fields = ["hostname"]

    def perform_create(self, serializer: DeviceSerializer) -> DeviceSerializer:
        serializer.save(owner=self.request.user)


class DirList(RestrictedListMixIn, generics.ListCreateAPIView):
    serializer_class = DirSerializer
    model = Directories

    filterset_fields = ["emulator__name", "device__machine_id", "path"]
    search_fields = ["emulator__name", "path"]


class DirDetail(RestrictedMixIn, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DirSerializer
    model = Directories


class UserDetail(generics.RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    model = User
    lookup_field = "username"

    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self, *args, **kwargs) -> QuerySet:
        if self.request.user.is_superuser:
            return self.model.objects.all()
        return User.objects.filter(id=self.request.user.id)
